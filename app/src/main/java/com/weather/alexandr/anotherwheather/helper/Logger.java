package com.weather.alexandr.anotherwheather.helper;

import android.util.Log;

/**
 * Created by onos on 11/11/2015.
 */
public class Logger {

    public static final boolean APP_DEBUG = true;
    private static final String TAG = "Weather APP";

    public static void logThisOne(String logMessage) {
        if (APP_DEBUG) Log.d(TAG, logMessage);
    }


}
