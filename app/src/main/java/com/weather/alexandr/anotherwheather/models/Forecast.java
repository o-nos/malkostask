package com.weather.alexandr.anotherwheather.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by alexandr on 09.04.15.
 */
public class Forecast {

    public City city;

    public List<MainTemp> list;

    public String getTemp(int i){
        return this.list.get(i).temperature.getCurrentTemperature();
    }

    public String getIcon(int i){
        return this.list.get(i).weather.get(0).getIconId();
    }

    public String getData(int i){
        return this.list.get(i).getData();
    }

    public String getDescription(int i){
        return this.list.get(i).weather.get(0).getDescription();
    }

}
