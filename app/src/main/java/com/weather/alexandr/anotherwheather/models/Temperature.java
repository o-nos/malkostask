package com.weather.alexandr.anotherwheather.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alexandr on 09.04.15.
 */
public class Temperature {

    @SerializedName("temp")
    String currentTemperature;

    @SerializedName("temp_min")
    String minTemperature;

    @SerializedName("temp_max")
    String maxTemperature;

    public String getMinTemperature() {
        return minTemperature;
    }

    public String getMaxTemperature() {
        return maxTemperature;
    }

    public void setCurrentTemperature(String currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public void setMinTemperature(String minTemperature) {
        this.minTemperature = minTemperature;
    }

    public void setMaxTemperature(String maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public String getCurrentTemperature() {
        return currentTemperature;

    }
}
