package com.weather.alexandr.anotherwheather.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alexandr on 09.04.15.
 */
public class City {

    @SerializedName("name")
    public String cityName;

    public String getCityName() {
        return cityName;
    }

}
