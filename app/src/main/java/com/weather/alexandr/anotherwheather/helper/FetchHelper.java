package com.weather.alexandr.anotherwheather.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.weather.alexandr.anotherwheather.models.Forecast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by onos on 11/23/2015.
 */
public class FetchHelper {

    public static final String ICON_LINK = "http://openweathermap.org/img/w/%s.png";
    public static final String FORECAST_CITY = "forecast_city";
    private static final String FORECAST_LINK = "http://api.openweathermap.org/data/2.5/forecast?q=%s&lang=ua&units=metric&mode=json&APPID=ac60152af45ff7e7c9250b18899a4653";

    /**
     * @param city forecast will be loaded for this city with request to OpenWeather API
     * @return JSON response with forecast
     */
    public static String fetchForecast(String city) {

        String forecastURL = String.format(FORECAST_LINK, city);

        try {
            URL fetchURL = new URL(forecastURL);
            HttpURLConnection connection =
                    (HttpURLConnection) fetchURL.openConnection();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            StringBuffer json = new StringBuffer(1024);
            String tmp = "";
            while ((tmp = reader.readLine()) != null)
                json.append(tmp).append("\n");
            reader.close();
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * @param linkFormat url with request to OpenWeather API to fetch icon for current forecast
     * @param forecast   current Forecast to be shown in the widget
     * @return icon Bitmap
     */
    public static Bitmap loadForecastImage(String linkFormat, Forecast forecast) {

        String iconLink = String.format(linkFormat, forecast.getIcon(0));

        try {
            URL url = new URL(iconLink);
            HttpURLConnection connection =
                    (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();

            InputStream inputStream = connection.getInputStream();
            return BitmapFactory.decodeStream(inputStream);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

}
