package com.weather.alexandr.anotherwheather.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.weather.alexandr.anotherwheather.helper.FetchHelper;
import com.weather.alexandr.anotherwheather.helper.Logger;
import com.weather.alexandr.anotherwheather.R;
import com.weather.alexandr.anotherwheather.helper.TextFormatHelper;
import com.weather.alexandr.anotherwheather.models.Forecast;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by alexandr on 09.04.15.
 */
public class ForecastAdapter extends ArrayAdapter<Forecast> {

    public ForecastAdapter(Context context, ArrayList<Forecast> forecastList) {
        super(context, 0, forecastList);
    }

    static class ViewHolder {
        @Bind(R.id.description_view) TextView descriptionView;
        @Bind(R.id.temp_view) TextView temperatureView;
        @Bind(R.id.icon_view) ImageView iconView;
        @Bind(R.id.data_view) TextView dataView;

        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        // якщо ще не отримали представлення, то заповнюємо його
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.forecast_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        try {
            Forecast forecast = getItem(position); // налаштування кожного окремого об’єкта

            Double temperature = Double.parseDouble(forecast.getTemp(position));

            viewHolder.temperatureView.setText(String.format(convertView.getResources().getString(R.string.celsius),
                    temperature.intValue()));
            viewHolder.descriptionView.setText(forecast.getDescription(position));
            viewHolder.dataView.setText(TextFormatHelper.makeUkrDataFromString(forecast.getData(position)));

            String imageLink = String.format(FetchHelper.ICON_LINK, forecast.getIcon(position));

            Picasso.with(getContext())
                    .load(imageLink)
                    .placeholder(R.drawable.placeholder)
                    .into(viewHolder.iconView);

        } catch (Exception e) {
            Logger.logThisOne(e.getMessage());
        }

        return convertView;
    }
}
