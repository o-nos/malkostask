package com.weather.alexandr.anotherwheather;

import android.app.Fragment;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.weather.alexandr.anotherwheather.adapters.ForecastAdapter;
import com.weather.alexandr.anotherwheather.helper.FetchHelper;
import com.weather.alexandr.anotherwheather.helper.Logger;
import com.weather.alexandr.anotherwheather.helper.TextFormatHelper;
import com.weather.alexandr.anotherwheather.models.Forecast;
import com.weather.alexandr.anotherwheather.widget.AnotherWeatherWidget;
import com.weather.alexandr.anotherwheather.widget.UpdateWidgetService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ForecastFragment extends Fragment {

    private SharedPreferences sharedPreferences;
    private ArrayList<Forecast> forecastList;
    private String resultJSON;

    @Bind(R.id.forecast_list_view)
    ListView listView;
    @Bind(android.R.id.progress)
    ProgressBar progress;
    @Bind(R.id.city_change_view)
    EditText cityToChange;
    @Bind(R.id.change_button)
    Button changeButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        ButterKnife.bind(this, rootView);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String city = TextFormatHelper.upperCaseCity(cityToChange.getText().toString().trim());
                if (!TextUtils.isEmpty(city) && getActivity() != null) {
                    new ForecastFetch().execute(city);

                    cityToChange.setText(city);

                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString(FetchHelper.FORECAST_CITY, city);
                    edit.apply();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String lastCity = sharedPreferences.getString(FetchHelper.FORECAST_CITY, getString(R.string.poltava));
        cityToChange.setHint(lastCity);

        new ForecastFetch().execute(lastCity);
    }

    private class ForecastFetch extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }

        @Override
        protected String doInBackground(String... urls) {

            return FetchHelper.fetchForecast(urls[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            resultJSON = result;
            Gson gson = new Gson();

            forecastList = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                Forecast f = gson.fromJson(resultJSON, Forecast.class);
                forecastList.add(f);
            }

            listView.setAdapter(new ForecastAdapter(getActivity(), forecastList));

            // check if we have at least one widget to be updated
            int ids[] = AppWidgetManager.getInstance(getActivity())
                    .getAppWidgetIds(new ComponentName(getActivity(), AnotherWeatherWidget.class));
            Logger.logThisOne("Widget count: " + ids.length);
            if (ids.length > 0) {
                updateWidget();
            }

            progress.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }

    }

    private void updateWidget() {
        Context context = getActivity();

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        ComponentName anotherWidget = new ComponentName(context, AnotherWeatherWidget.class);

        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(anotherWidget);

        Intent intent = new Intent(context.getApplicationContext(),
                UpdateWidgetService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);

        context.startService(intent);
    }

}
