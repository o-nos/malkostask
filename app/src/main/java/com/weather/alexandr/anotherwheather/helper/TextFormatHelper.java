package com.weather.alexandr.anotherwheather.helper;

import android.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by onos on 11/16/2015.
 */
public class TextFormatHelper {

    public static String upperCaseCity(String city) {

        if (!TextUtils.isEmpty(city)) {

            char[] stringArray = city.trim().toCharArray();
            stringArray[0] = Character.toUpperCase(stringArray[0]);

            return new String(stringArray).trim();
        }

        return null;
    }

    public static String makeUkrDataFromString(String data) {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
            return data; // повертаю ту ж саму строку, щоб не було пустої
        }

        DateFormat uaFormat = new SimpleDateFormat("dd.MM EEE HH:mm", new Locale("uk", "UA"));
        return uaFormat.format(date);
    }

}
