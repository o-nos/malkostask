package com.weather.alexandr.anotherwheather.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by alexandr on 09.04.15.
 */
public class MainTemp {

    @SerializedName("main")
    public Temperature temperature;

    public List<Weather> weather;

    @SerializedName("dt_txt")
    public String data;

    public String getData() {
        return data;
    }
}
