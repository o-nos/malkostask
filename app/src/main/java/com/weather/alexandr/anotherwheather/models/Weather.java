package com.weather.alexandr.anotherwheather.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alexandr on 09.04.15.
 */
public class Weather {

    @SerializedName("main")
    public String condition; // англійською

    @SerializedName("description")
    public String description; // мовою. котра вказана в ссилці

    @SerializedName("icon")
    public String iconId;

    public String getIconId(){
        return iconId;
    }

    public String getCondition() {
        return condition;
    }

    public String getDescription() {
        return description;
    }
}
