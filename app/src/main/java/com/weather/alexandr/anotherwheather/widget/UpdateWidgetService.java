package com.weather.alexandr.anotherwheather.widget;

import android.app.IntentService;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.widget.RemoteViews;

import com.google.gson.Gson;
import com.weather.alexandr.anotherwheather.helper.FetchHelper;
import com.weather.alexandr.anotherwheather.helper.Logger;
import com.weather.alexandr.anotherwheather.MainActivity;
import com.weather.alexandr.anotherwheather.R;
import com.weather.alexandr.anotherwheather.helper.TextFormatHelper;
import com.weather.alexandr.anotherwheather.models.Forecast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by onos on 11/11/2015.
 */
public class UpdateWidgetService extends IntentService {

    public UpdateWidgetService() {
        super("UpdateWidgetService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Logger.logThisOne("onHandleIntent() for widget is called");

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String city = TextFormatHelper.upperCaseCity(
                sharedPreferences.getString(FetchHelper.FORECAST_CITY, getString(R.string.poltava)));

        // background deal, loading forecast
        Forecast currentForecast = loadForecast(city);
        Bitmap forecastImage = FetchHelper.loadForecastImage(FetchHelper.ICON_LINK, currentForecast);
        Double temperature = Double.parseDouble(currentForecast.getTemp(0));

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this
                .getApplicationContext());

        int[] allWidgetIds = intent
                .getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);

        for (int widgetId : allWidgetIds) {

            RemoteViews remoteViews = new RemoteViews(this
                    .getApplicationContext().getPackageName(),
                    R.layout.widget_layout);

            remoteViews.setImageViewBitmap(R.id.icon_view, forecastImage);

            remoteViews.setTextViewText(R.id.description_text, currentForecast.getDescription(0));
            remoteViews.setTextViewText(R.id.city_text,
                    city);

            remoteViews.setTextViewText(R.id.temperature_text,
                    String.format(getString(R.string.celsius), temperature.intValue()));
            // привожу до інту, щоб показувало ціле значення

            Intent clickIntent = new Intent(getApplicationContext(),
                    MainActivity.class);

            clickIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent configPendingIntent =
                    PendingIntent.getActivity(getApplicationContext(), 0, clickIntent, 0);

            remoteViews.setOnClickPendingIntent(R.id.widget_layout, configPendingIntent);

            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }

    }

    private Forecast loadForecast(String city) {

        String forecastJson = FetchHelper.fetchForecast(city);

        if (!TextUtils.isEmpty(forecastJson)) {
            Gson gson = new Gson();

            return gson.fromJson(forecastJson, Forecast.class);
        }
        return new Forecast();
    }

}

